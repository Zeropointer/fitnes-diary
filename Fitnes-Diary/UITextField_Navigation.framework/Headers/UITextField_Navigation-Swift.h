// Generated by Apple Swift version 3.0.2 (swiftlang-800.0.63 clang-800.0.42.1)
#pragma clang diagnostic push

#if defined(__has_include) && __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wauto-import"
#include <objc/NSObject.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if defined(__has_include) && __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus) || __cplusplus < 201103L
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
typedef unsigned int swift_uint2  __attribute__((__ext_vector_type__(2)));
typedef unsigned int swift_uint3  __attribute__((__ext_vector_type__(3)));
typedef unsigned int swift_uint4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif
#if !defined(SWIFT_CLASS_PROPERTY)
# if __has_feature(objc_class_property)
#  define SWIFT_CLASS_PROPERTY(...) __VA_ARGS__
# else
#  define SWIFT_CLASS_PROPERTY(...)
# endif
#endif

#if defined(__has_attribute) && __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if defined(__has_attribute) && __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if defined(__has_attribute) && __has_attribute(objc_method_family)
# define SWIFT_METHOD_FAMILY(X) __attribute__((objc_method_family(X)))
#else
# define SWIFT_METHOD_FAMILY(X)
#endif
#if defined(__has_attribute) && __has_attribute(noescape)
# define SWIFT_NOESCAPE __attribute__((noescape))
#else
# define SWIFT_NOESCAPE
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if defined(__has_attribute) && __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if defined(__has_attribute) && __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name) enum _name : _type _name; enum SWIFT_ENUM_EXTRA _name : _type
# if defined(__has_feature) && __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) SWIFT_ENUM(_type, _name)
# endif
#endif
#if !defined(SWIFT_UNAVAILABLE)
# define SWIFT_UNAVAILABLE __attribute__((unavailable))
#endif
#if defined(__has_feature) && __has_feature(modules)
@import UIKit;
@import CoreGraphics;
#endif

#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
@class NavigationFieldToolbar;
@class UIView;

/**
  Protocol for navigation field.
*/
SWIFT_PROTOCOL("_TtP22UITextField_Navigation15NavigationField_")
@protocol NavigationField
/**
  The next navigation field.
*/
@property (nonatomic, weak) id <NavigationField> _Nullable nextNavigationField;
/**
  The previous navigation field.
*/
@property (nonatomic, readonly, weak) id <NavigationField> _Nullable previousNavigationField;
/**
  The toolbar on the keyboard for the receiver.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbar * _Nullable navigationFieldToolbar;
/**
  The input accessory view.
*/
@property (nonatomic, strong) UIView * _Nullable inputAccessoryView;
/**
  Make the receiver become first responder.
*/
- (BOOL)becomeFirstResponder;
/**
  Make the receiver resign first responder.
*/
- (BOOL)resignFirstResponder;
/**
  Force applying the \code
  navigationFieldToolbar
  \endcode to the keyboard for the receiver.
*/
- (void)applyInputAccessoryView;
@end


/**
  Protocol for delegate of \code
  NavigationField
  \endcode.
*/
SWIFT_PROTOCOL("_TtP22UITextField_Navigation23NavigationFieldDelegate_")
@protocol NavigationFieldDelegate
@optional
/**
  Tells the \code
  delegate
  \endcode that the previous button was tapped. The \code
  delegate
  \endcode has to make the \code
  previousNavigationField
  \endcode \code
  becomeFirstResponder
  \endcode if needed.
  \param navigationField the navigation field whose \code
  inputAccessoryView
  \endcode’s button was tapped.

*/
- (void)navigationFieldDidTapPreviousButton:(id <NavigationField> _Nonnull)navigationField;
/**
  Tells the \code
  delegate
  \endcode that the next button was tapped. The \code
  delegate
  \endcode has to make the \code
  nextNavigationField
  \endcode \code
  becomeFirstResponder
  \endcode if needed.
  \param navigationField the navigation field whose \code
  inputAccessoryView
  \endcode’s button was tapped.

*/
- (void)navigationFieldDidTapNextButton:(id <NavigationField> _Nonnull)navigationField;
/**
  Tells the \code
  delegate
  \endcode that the done button was tapped. The \code
  delegate
  \endcode has to make the navigation field \code
  resignFirstResponder
  \endcode if needed.
  \param navigationField the navigation field whose \code
  inputAccessoryView
  \endcode’s button was tapped.

*/
- (void)navigationFieldDidTapDoneButton:(id <NavigationField> _Nonnull)navigationField;
@end

@class NavigationFieldToolbarButtonItem;
@class NSCoder;

/**
  Class for the \code
  inputAccessoryView
  \endcode.
*/
SWIFT_CLASS("_TtC22UITextField_Navigation22NavigationFieldToolbar")
@interface NavigationFieldToolbar : UIToolbar
/**
  Holds the previous button.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbarButtonItem * _Nonnull previousButton;
/**
  Holds the next button.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbarButtonItem * _Nonnull nextButton;
/**
  Holds the done button.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbarButtonItem * _Nonnull doneButton;
/**
  Has not been implemented. Use \code
  init()
  \endcode instead.
*/
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
- (nonnull instancetype)initWithFrame:(CGRect)frame SWIFT_UNAVAILABLE;
@end


/**
  Class for the previous, next and done buttons. Created for easily setting \code
  UIAppearance
  \endcode.
*/
SWIFT_CLASS("_TtC22UITextField_Navigation32NavigationFieldToolbarButtonItem")
@interface NavigationFieldToolbarButtonItem : UIBarButtonItem
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
@end


@interface UITextField (SWIFT_EXTENSION(UITextField_Navigation))
@end


@interface UITextField (SWIFT_EXTENSION(UITextField_Navigation)) <NavigationField>
/**
  The next navigation field. Setting this will also set the \code
  previousNavigationField
  \endcode on the assigned navigation field.
*/
@property (nonatomic, weak) IBOutlet id <NavigationField> _Nullable nextNavigationField;
/**
  The previous navigation field. This is set automatically on the navigation field which you assign the receiver as \code
  nextNavigationField
  \endcode.
*/
@property (nonatomic, readonly, weak) id <NavigationField> _Nullable previousNavigationField;
/**
  Returns the \code
  inputAccessoryView
  \endcode if it is a \code
  NavigationFieldToolbar
  \endcode. Otherwise, returns \code
  nil
  \endcode.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbar * _Nullable navigationFieldToolbar;
/**
  Apply the \code
  inputAccessoryView
  \endcode. Useful when you want the Done button but the receiver does not have any next or previous navigation fields.
*/
- (void)applyInputAccessoryView;
@end


@interface UITextView (SWIFT_EXTENSION(UITextField_Navigation))
@end


@interface UITextView (SWIFT_EXTENSION(UITextField_Navigation)) <NavigationField>
/**
  The next navigation field. Setting this will also set the \code
  previousNavigationField
  \endcode on the assigned navigation field.
*/
@property (nonatomic, weak) IBOutlet id <NavigationField> _Nullable nextNavigationField;
/**
  The previous navigation field. This is set automatically on the navigation field which you assign the receiver as \code
  nextNavigationField
  \endcode.
*/
@property (nonatomic, readonly, weak) id <NavigationField> _Nullable previousNavigationField;
/**
  Returns the \code
  inputAccessoryView
  \endcode if it is a \code
  NavigationFieldToolbar
  \endcode. Otherwise, returns \code
  nil
  \endcode.
*/
@property (nonatomic, readonly, strong) NavigationFieldToolbar * _Nullable navigationFieldToolbar;
/**
  Apply the \code
  inputAccessoryView
  \endcode. Useful when you want the Done button but the receiver does not have any next or previous navigation fields.
*/
- (void)applyInputAccessoryView;
@end

#pragma clang diagnostic pop
