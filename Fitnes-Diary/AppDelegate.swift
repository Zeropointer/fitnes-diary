//
//  AppDelegate.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 27.01.17.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: ApplicationServicesManager {
    
    override var services: [ApplicationService] {
        return [
            LoggerApplicationService(),
            AnalyticsService(),
            CustomApperanceService(),
            URLHandlingService()
        ]
    }
}

