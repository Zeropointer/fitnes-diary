//
//  FitnesDevice.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 27.01.17.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import Foundation
import RealmSwift

class FitnessDevice: Object {
    
    dynamic var identifier = NSUUID().uuidString
    
    dynamic var name : String = ""
    
    dynamic var deviceNumber : String = ""
    
    var minWeight = RealmOptional<Int>()
    
    var maxWeight = RealmOptional<Int>()
    
    var weightSteps = RealmOptional<Int>()
    
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
    override static func indexedProperties() -> [String] {
        return ["name"]
    }
}
