//
//  FitnessDay.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 27.01.17.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import Foundation
import RealmSwift

class FitnessDay: Object {
    
    dynamic var date : Date?
    
    var exercises = List<FitnessExercise>()
 
    
    
}
