//
//  FitnessExercise.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 27.01.17.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import Foundation
import RealmSwift


class FitnessExercise: Object {
    
    var weight = RealmOptional<Int>()
    
    dynamic var exerciseName : String = ""
    
    dynamic var counts = 0
    
    dynamic var sessions = 0
    
    dynamic var device : FitnessDevice?
    
}
