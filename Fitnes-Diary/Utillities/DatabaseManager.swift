//
//  PSDataBaseManager.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 27.01.17.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    
    private init() {
        
    }
    
    func add(object : Object, update : Bool = true) throws {
        let realm = try Realm()
        try realm.write {
            realm.add(object, update: update)
        }
    }
    
    func add<S : Sequence>(_ objects: S, update: Bool = true) throws where S.Iterator.Element : Object {
        let realm = try Realm()
        try realm.write {
            realm.add(objects, update: update)
        }
    }
    
    func delete(object : Object) throws {
        let realm = try Realm()
        try realm.write {
            realm.delete(object)
        }
    }
    
    func delete<S : Sequence>(_ objects: S) throws where S.Iterator.Element : Object {
        let realm = try Realm()
        try realm.write {
            realm.delete(objects)
        }
    }
}
