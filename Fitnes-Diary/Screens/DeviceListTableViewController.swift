//
//  DeviceListTableViewController.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 02/03/2017.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class DeviceListTableViewController : UITableViewController {
    
    let results : Results<FitnessDevice> = try! Realm().objects(FitnessDevice.self)
    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notificationToken = results.addNotificationBlock { (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self.tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the TableView
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: insertions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self.tableView.deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self.tableView.reloadRows(at: modifications.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self.tableView.endUpdates()
                break
            case .error(let err):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(err)")
                break
            }
        }

        
        self.setupNavigationsItems()
    }
    
    private func setupNavigationsItems() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItem))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    func addItem() {
        self.performSegue(withIdentifier: "addDevice", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addDevice" {
            var device : FitnessDevice?
            if let indexPath = sender as? IndexPath {
                device = self.results[indexPath.row]
            }
            
            if let detailVc = segue.destination as? DeviceAddViewController {
                detailVc.fitDevice = device
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceCellIdentifier") else {
            return UITableViewCell()
        }
        let device = self.results[indexPath.row]
        
        cell.textLabel?.text = device.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "addDevice", sender: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let device = self.results[indexPath.row]
            try? self.results.realm?.write {
                
                self.results.realm?.delete(device)
            }
        }
    }
}
