//
//  DeviceAddViewController.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 02/03/2017.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import UIKit
import UITextField_Navigation

class DeviceAddViewController: UIViewController {

    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var deviceNameTextField: UITextField!
    @IBOutlet weak var deviceNumberTextField: UITextField!
    @IBOutlet weak var minWeightTextField: UITextField!
    @IBOutlet weak var maxWeightTextField: UITextField!
    @IBOutlet weak var weightStepsTextField: UITextField!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    var fitDevice : FitnessDevice?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.deviceNameTextField.nextNavigationField = self.deviceNumberTextField
        self.deviceNumberTextField.nextNavigationField = self.minWeightTextField
        self.minWeightTextField.nextNavigationField = self.maxWeightTextField
        self.maxWeightTextField.nextNavigationField = self.weightStepsTextField
        /*
        self.weightStepsTextField.previousNavigationField = self.maxWeightTextField
        self.maxWeightTextField.previousNavigationField = self.minWeightTextField
        self.minWeightTextField.previousNavigationField = self.deviceNumberTextField
        self.deviceNumberTextField.previousNavigationField = self.deviceNameTextField
        */
        
        self.deviceNameTextField.text = self.fitDevice?.name
        self.deviceNumberTextField.text = self.fitDevice?.deviceNumber
        if let minWeight = self.fitDevice?.minWeight.value {
            self.minWeightTextField.text = String(minWeight)
        }
        
        if let maxWeight = self.fitDevice?.maxWeight.value {
            self.maxWeightTextField.text = String(maxWeight)
        }
        if let weightSteps = self.fitDevice?.weightSteps.value {
            self.weightStepsTextField.text = String(weightSteps)
        }

        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] (note) in
            self?.scrollViewBottomConstraint.constant = 0.0
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] (note) in
            guard let kbEndFrame = (note.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
            }
            
            if let kbEndFrameInView = self?.view.convert(kbEndFrame, from: UIApplication.shared.keyWindow) {
                self?.scrollViewBottomConstraint.constant = kbEndFrameInView.size.height
            }
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let name = self.deviceNameTextField.text,
            let _minWeight = self.minWeightTextField.text,
            let _maxWeight = self.maxWeightTextField.text,
            let _weightSteps = self.weightStepsTextField.text,
            let minWeight = Int(_minWeight), let maxWeight = Int(_maxWeight), let weightSteps = Int(_weightSteps){
            
            
            
            let device  = FitnessDevice()
            var update = false
            if let fitDevice = self.fitDevice {
                device.identifier = fitDevice.identifier
                update = true
            }
            device.name = name
            if let number = self.deviceNumberTextField.text {
                device.deviceNumber = number
            }
            device.minWeight.value = minWeight
            
            device.maxWeight.value = maxWeight
            device.weightSteps.value = weightSteps
            
            try? DatabaseManager.shared.add(object: device, update: update)
        }
        
        
        
    }

    @objc func dismissKeyBoard(){
        self.deviceNameTextField.resignFirstResponder()
        self.deviceNumberTextField.resignFirstResponder()
        self.minWeightTextField.resignFirstResponder()
        self.maxWeightTextField.resignFirstResponder()
        self.weightStepsTextField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing: \(textField.placeholder)")
    }
}

extension ViewController: NavigationFieldDelegate {
    
    func navigationFieldDidTapPreviousButton(_ navigationField: NavigationField) {
        print("navigationFieldDidTapPreviousButton: \(navigationField)")
        navigationField.previousNavigationField?.becomeFirstResponder()
    }
    
    func navigationFieldDidTapNextButton(_ navigationField: NavigationField) {
        print("navigationFieldDidTapNextButton: \(navigationField)")
        navigationField.nextNavigationField?.becomeFirstResponder()
    }
    
    func navigationFieldDidTapDoneButton(_ navigationField: NavigationField) {
        print("navigationFieldDidTapDoneButton: \(navigationField)")
        navigationField.resignFirstResponder()
    }
}
