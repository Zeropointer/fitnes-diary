//
//  CustomAppearanceService.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 14/03/2017.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import UIKit

final class CustomApperanceService: NSObject, ApplicationService {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        print("It has started!")
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("It has entered background")
    }
}
