//
//  URLHandlingService.swift
//  Fitnes-Diary
//
//  Created by Patrick Singer on 14/03/2017.
//  Copyright © 2017 Patrick Singer. All rights reserved.
//

import UIKit

final class URLHandlingService: NSObject, ApplicationService {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        print("It has started!")
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("It has entered background")
    }
    
    public func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return true
    }
    
    public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return true
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }
}
